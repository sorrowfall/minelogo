import { json, error } from '@sveltejs/kit'

import { Model, Row, Text } from '$lib/generate'

interface RowData {

    model: string
    textPosition: string
    textRotation: string

    texts: Array<TextData>

    finalTexts?: Text[]

}

interface TextData {

    text: string
    model: string
    texture: string

}

export function GET( event ) {

    let rawArgs = decodeURI(event.url.search.split('?')[1])
    let rows

    try {
        rows = rawArgs.split('&').map(i => JSON.parse(i))
    } catch (SyntaxError) {
        throw error(451, 'Invalid JSON data. For more information, check the <a href="">documentation</a>.')
    }

    let finalRows: Row[] = []

    for (let row of rows) {

        row.finalTexts = []

        for (let text of row.texts) {

            row.finalTexts.push(new Text(text.text, text.texture, text.model))

        }

        finalRows.push(new Row(row.finalTexts, row.model, row.textPosition, row.textRotation))

    }

    const model = new Model(finalRows)

    return json( model.toJSON() )

    let resp = new Response( JSON.stringify(model.toJSON()) )
    resp.headers.set("Content-type", "application/json")
    resp.headers.set("Content-Disposition", "attachment; filename=model.mimodel")

    return resp

}