# Minelogo

## To-Do

- Implement .mimodel rotations
- Implement .mimodel inflations

## Special Thanks

### [Yanuar Mohendra]()

Made the original [letter models](https://www.mineimatorforums.com/index.php?/topic/76334-yms-minecraft-letters-model/).

### [IllagerCaptain](https://discordapp.com/users/328661975250894850)

Inspiration for the UI of the site.
