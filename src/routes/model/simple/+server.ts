import { json, error } from '@sveltejs/kit'

import { Model, Row, Text } from '$lib/generate'

export function GET( event ) {

    let rawArgs = decodeURI(event.url.search.split('?')[1])

    const model = new Model( [ new Row( [ new Text(rawArgs) ] ) ] )

    let resp = new Response( JSON.stringify(model.toJSON()) )
    resp.headers.set("Content-type", "application/json")
    resp.headers.set("Content-Disposition", `attachment; filename=${rawArgs}.mimodel`)

    return resp

}